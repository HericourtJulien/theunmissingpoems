<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Poem;
use AppBundle\Entity\Poems;
use AppBundle\Entity\UserAlt;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            //$session = $request->getSession();

            $user = $this->getUser();

            // get all poems
            $poems = new Poems();
            $userAlt = new UserAlt($this->getUser()->getId());

            $poem = $poems->getPoemById($userAlt->getPoemId());

            $fragment = $poem->getFragmentById($userAlt->getFragmentId());

            $answerState = true;

            if (!empty($_POST["answerRiddle"])) {

                $answerRiddle = $_POST["answerRiddle"];

                if (strtolower($answerRiddle) == strtolower($fragment->getAnswer())) {
                    $step = $userAlt->getStep();
                    $userAlt->setStep(intval($step + 1));

                    $data['fragment_id'] = $userAlt->getFragmentId();
                    $data['step'] = $userAlt->getStep();
                    $data['poem_id'] = $userAlt->getPoemId();

                    $userAlt->updateDB($data);

                } else {
                    $answerState = false;
                }
            }

            if (!empty($_POST["answerCode"])) {

                $answerCode = $_POST["answerCode"];

                if ($answerCode == $fragment->getCode()) {

                    // On reset le step
                    $userAlt->setStep(0);

                    $nextFragmentId  = $userAlt->getFragmentId() + 1;

                    // Si l'id du fragment n'existe pas
                    if($nextFragmentId > count($poem->getFragments())) {

                        //$userAlt->setFragmentId(1);
                        //$nextFragmentId = $userAlt->getFragmentId();*/

                        // On recupere l'index du poeme actuel
                        $poemIndex = array_search($poem, $poems->getPoems());

                        $nextPoemIndex = $poemIndex + 1;

                        // Si il n'y a pas de poeme suivant, c'est gagné
                        $poemsDatas = $poems->getPoems();

                        if(!isset($poemsDatas[$nextPoemIndex])) {
                            rekt("c'est gagné !");
                        } else {
                            // On recup le nouveau poeme
                            $poem = $poems->getPoem($nextPoemIndex);

                            $userAlt->setPoemId($poem->getId());

                            // On reset le step
                            $step = 0;
                            $userAlt->setStep($step);

                            for($i = 0; $i < count($poem->getFragments()); $i++) {
                                $tempFragment = $poem->getFragment($i);

                                if($tempFragment->getOrder() == 1) {
                                    $userAlt->setFragmentId($tempFragment->getId());
                                }
                            }

                            $fragment = $poem->getFragmentById($userAlt->getFragmentId());
                        }

                    } else {

                        // le fragment existe dans le poeme actuel

                        // On passe au fragment suivant
                        $userAlt->setFragmentId($nextFragmentId);

                        // On récupère le nouveau fragment en cours
                        $fragment = $poem->getFragmentById($userAlt->getFragmentId());
                    }

                    $data['fragment_id'] = $userAlt->getFragmentId();
                    $data['step'] = $userAlt->getStep();
                    $data['poem_id'] = $userAlt->getPoemId();

                    $userAlt->updateDB($data);

                } else {
                    $answerState = false;
                }
            }

            // Getting discovered fragment values
            $fragmentDiscovered = [];

            $nbrFragmentDiscovered = $poem->getFragmentById($userAlt->getFragmentId())->getOrder() - 1;

            for($i = 0; $i < $nbrFragmentDiscovered; $i++) {
                $tempFragment = $poem->getFragment($i);
                if($tempFragment->getOrder() <= $nbrFragmentDiscovered) {
                    $fragmentDiscovered[$i] = $tempFragment->getValue();
                }
            }

            if (!empty($_POST["sendRiddle"])) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('UnmissingPoems')
                    ->setFrom('mrtriickyx@gmail.com')
                    ->setTo($this->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/riddle.html.twig
                            'Emails/riddle.html.twig',
                            array(
                                'riddle' => $fragment->getRiddle(),
                                'username' => $this->getUser()->getUsername()
                            )
                        ),
                        'text/html'
                    )
                ;
                $this->get('mailer')->send($message);
            }



            return $this->render('AppBundle:Interfaces:home.html.twig', [
                'step' => $userAlt->getStep(),
                'poem' => $poem,
                'fragment' => $fragment,
                'user' => $user,
                'answerState' => $answerState,
                'fragmentDiscovered' => $fragmentDiscovered
            ]);

        } else {
            return $this->redirect("/login");
        }
    }
}



function rekt($rekt) {
    echo '<pre>' + var_dump($rekt) + '</pre>';
    die();
}
