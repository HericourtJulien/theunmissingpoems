<?php

namespace AppBundle\Entity;


use PDO;

class Fragment
{
    private $id;
    private $poemId;
    private $order;
    private $lat;
    private $long;
    private $riddle;
    private $answer;
    private $code;
    private $value;

    /**
     * Fragment constructor.
     */
    public function __construct($data)
    {
        $this->build($data);
    }

    private function build($data) {

        $this->setId($data['id']);
        $this->setPoemId($data['poem_id']);
        $this->setOrder($data['order']);
        $this->setLat($data['lat']);
        $this->setLong($data['long']);
        $this->setRiddle($data['riddle']);
        $this->setAnswer($data['answer']);
        $this->setCode($data['code']);
        $this->setValue($data['value']);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = intval($id);
    }

    /**
     * @return mixed
     */
    public function getPoemId()
    {
        return $this->poemId;
    }

    /**
     * @param mixed $poemId
     */
    public function setPoemId($poemId)
    {
        $this->poemId = intval($poemId);
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = intval($order);
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat)
    {
        $this->lat = intval($lat);
    }

    /**
     * @return mixed
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param mixed $long
     */
    public function setLong($long)
    {
        $this->long = intval($long);
    }

    /**
     * @return mixed
     */
    public function getRiddle()
    {
        return $this->riddle;
    }

    /**
     * @param mixed $riddle
     */
    public function setRiddle($riddle)
    {
        $this->riddle = $riddle;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param mixed $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = intval($code);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}