<?php

namespace AppBundle\Entity;


use PDO;

class Poem
{
    private $id;
    private $name;
    private $fragments = [];
    private $pdo;

    /**
     * Poeme constructor.
     * @param $id
     */
    public function __construct($data)
    {
        $this->setId($data['id']);

        $this->setName($data['name']);

        $this->fetchDB();
    }

    private function fetchDB() {

        $this->setPdo();

        // Set tableau de fragment
        $req = $this->getPdo()->prepare('SELECT * FROM fragment WHERE (poem_id = :poem_id) ORDER BY "order"');

        $req->execute([
            'poem_id' => $this->getId(),
        ]);

        $rep = $req->fetchAll();

        $this->setFragments($rep);

        $this->closePdo();
    }

    /**
     * @param mixed $id
     */
    private function setId($id)
    {
        $this->id = intval($id);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFragment($index)
    {
        return $this->fragments[$index];
    }

    /**
     * @return mixed
     */
    public function getFragmentById($fragmentId)
    {
        for($i = 0; $i < count($this->getFragments()); $i++) {
            $tempFragment = $this->getFragment($i);

            if($tempFragment->getId() == $fragmentId) {
                return $tempFragment;
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getFragments()
    {
        return $this->fragments;
    }

    /**
     * @internal param fragment $fragments
     */
    private function setFragments($data)
    {
        for($i = 0; $i < count($data); $i++) {
            array_push($this->fragments, new Fragment($data[$i]));
        }
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return pdo
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @internal param PDO $pdo
     */
    public function setPdo()
    {
        $this->pdo = new PDO('mysql:dbname=unmissingpoems;host=127.0.0.1;port=8889', 'root', 'root');

        //si erreur avec PDO, renvoit une erreur (de base, PDO ne dit rien si erreur)
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @internal param PDO $pdo
     */
    public function closePdo()
    {
        $this->pdo = null;
    }
}