<?php
/**
 * Created by PhpStorm.
 * User: Sko
 * Date: 29/11/2016
 * Time: 19:29
 */

namespace AppBundle\Entity;


use PDO;

class Poems
{
    private $poems = [];
    private $pdo;

    /**
     * Poeme constructor.
     */
    public function __construct()
    {
        $this->fetchDB();
    }

    private function fetchDB() {

        $this->setPdo();
        $req = $this->getPdo()->prepare('SELECT * FROM poem');
        $req->execute();
        $rep = $req->fetchAll();

        $this->setPoems($rep);

        $this->closePdo();
    }

    /**
     * @return pdo
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @internal param PDO $pdo
     */
    public function setPdo()
    {
        $this->pdo = new PDO('mysql:dbname=unmissingpoems;host=127.0.0.1;port=8889', 'root', 'root');

        //si erreur avec PDO, renvoit une erreur (de base, PDO ne dit rien si erreur)
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @internal param PDO $pdo
     */
    public function closePdo()
    {
        $this->pdo = null;
    }


    /**
     * @return mixed
     */
    public function getPoem($index)
    {
        return $this->poems[$index];
    }

    /**
     * @return mixed
     */
    public function getPoemById($poemId)
    {
        for($i = 0; $i < count($this->getPoems()); $i++) {
            $tempPoem = $this->getPoem($i);
            if($tempPoem->getId() == $poemId) {
                return $tempPoem;
            }
        }
    }



    /**
     * @return array
     */
    public function getPoems()
    {
        return $this->poems;
    }

    /**
     * @param array
     */
    public function setPoems($data)
    {
        for($i = 0; $i < count($data); $i++) {
            array_push($this->poems, new Poem($data[$i]));
        }
    }
}