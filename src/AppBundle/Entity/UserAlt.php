<?php
/**
 * Created by PhpStorm.
 * User: Sko
 * Date: 28/11/2016
 * Time: 22:55
 */

namespace AppBundle\Entity;


use PDO;

class UserAlt
{

    private $userId;
    private $fragmentId;
    private $step;
    private $poemId;
    private $pdo;

    /**
     * UserStep constructor.
     */
    public function __construct($id)
    {

        $this->userId = $id;

        $this->fetchDB();
    }

    private function fetchDB() {

        $this->setPdo();

        $req = $this->getPdo()->prepare('SELECT * FROM user_alt WHERE user_id = :user_id');

        $req->execute([
            'user_id' => $this->getUserId()
        ]);

        $rep = $req->fetch();

        $this->setPoemId($rep['poem_id']);
        $this->setFragmentId($rep['fragment_id']);
        $this->setStep($rep['step']);

        $this->closePdo();
    }

    public function updateDB($data) {

        $this->setPdo();

        $req = $this->getPdo()->prepare('UPDATE user_alt SET fragment_id = :fragment_id, step = :step, poem_id = :poem_id WHERE user_id = :user_id');

        $req->execute([
            'user_id' => $this->getUserId(),
            'fragment_id' => $data['fragment_id'],
            'step' => $data['step'],
            'poem_id' => $data['poem_id']
        ]);

        $this->closePdo();
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = intval($userId);
    }

    /**
     * @return mixed
     */
    public function getFragmentId()
    {
        return $this->fragmentId;
    }

    /**
     * @param mixed $fragmentId
     */
    public function setFragmentId($fragmentId)
    {
        $this->fragmentId = intval($fragmentId);
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     */
    public function setStep($step)
    {
        $this->step = intval($step);
    }

    /**
     * @return mixed
     */
    public function getPoemId()
    {
        return $this->poemId;
    }

    /**
     * @param mixed $poemId
     */
    public function setPoemId($poemId)
    {
        $this->poemId = intval($poemId);
    }

    /**
     * @return pdo
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @internal param PDO $pdo
     */
    public function setPdo()
    {
        $this->pdo = new PDO('mysql:dbname=unmissingpoems;host=127.0.0.1;port=8889', 'root', 'root');

        //si erreur avec PDO, renvoit une erreur (de base, PDO ne dit rien si erreur)
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @internal param PDO $pdo
     */
    public function closePdo()
    {
        $this->pdo = null;
    }
}